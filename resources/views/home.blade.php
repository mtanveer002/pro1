@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a href="{{route('admin.company.index')}}" class="btn btn-success">Company</a>
                    <a href="{{route('admin.employees.index')}}" class="btn btn-success">Empolyees</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
