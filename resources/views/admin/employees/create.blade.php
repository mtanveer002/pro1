@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Add New employees') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                       
                    <form action="{{ route('admin.employees.store')}}" method="POST" enctype="multipart/form-data">              
                        @csrf
                       
                        <div class="mb-3">
                            <select name="company_id" class="form-select" aria-label="Default select example">
                                <option selected>Select Company</option>
                                @foreach ($company as $com)
                                    <option value="{{$com->id}}" >{{$com->name}}</option>    
                                @endforeach
                              </select>
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">First Name</label>
                            <input type="text" class="form-control" name="fname" id="slider_describtion" placeholder="employees First Name" required>
                        </div>
                        @error('fname')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Last Name</label>
                            <input type="text" class="form-control" name="lname" id="slider_describtion" placeholder="employees Last Name" required>
                        </div>
                        @error('lname')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label"> Email</label>
                            <input type="email" class="form-control" name="email" id="slider_describtion" placeholder="employees Email" required>
                        </div>
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Phone</label>
                            <input type="text" class="form-control" name="phone" id="slider_describtion" placeholder="employees phone" required>
                        </div>
                        @error('phone')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="mb-3 float-right">
                            <button type="submit" class="btn btn-success">Save</button>
                           
                        </div>
                    </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
