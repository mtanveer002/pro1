@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <strong>{{ session('success')}}</strong> 
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('employees') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="{{route('admin.employees.create')}}" class="btn btn-success float-right">Add New employees</a>
                        <br>
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Company</th>
                            <th scope="col">employees Phone</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($employees as $emp)
                                <tr>
                                    <th>{{ $employees->firstItem()+$loop->index}}</th>
                                    <td>{{$emp->name}}</td>
                                    <td>{{$emp->last_name}}</td>
                                    <td>{{$emp->email}}</td>
                                    <td>{{$emp->company_id }}</td>
                                    <td>{{$emp->phone}}</td>
                                    <td>
                                        <a href="{{route('admin.employees.edit',$emp->id)}}" class="btn btn-success">Edit</a>
                                       <form action="{{route('admin.employees.destroy',$emp)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger" >Delete</button>
                                       </form>
                                        
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                      {{ $employees->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
