@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <strong>{{ session('success')}}</strong> 
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('Company') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <input type="text" name="search" id="search" class="form-control col-6" placeholder="search">
                    <a href="{{route('admin.company.create')}}" class="btn btn-success float-right">Add New Company</a>
                        <br>
                    {{-- <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Company Name</th>
                            <th scope="col">Company Email</th>
                            <th scope="col">Company Logo</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>
                            @foreach ($company as $com)
                                <tr>
                                    <th scope="row">{{ $company->firstItem()+$loop->index}}</th>
                                    <td>{{$com->name}}</td>
                                    <td>{{$com->email}}</td>
                                    <td><img src="{{$com->getImage()}}" alt="" style="width: 100px; height:70px;"></td>
                                    <td>
                                        <a href="{{route('admin.company.edit',$com->id)}}" class="btn btn-success">Edit</a>
                                       <form action="{{route('admin.company.destroy',$com)}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger" >Delete</button>
                                       </form>
                                        
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                      {{ $company->links() }} --}}
                
                
                    </div>
                    <div id="search_list"></div>
            </div>
        </div>
    </div>
</div>
@endsection

<script>

    $(document).ready(function()){
        $('#search').on('keyup', function(){
            var query = $(this).val();
            $.ajax({
                url:"search",
                type: "GET",
                date:{'search':query},
                success:function(data){
                    $('#search_list').html(data);
                }
            });
        });
    }

</script>
