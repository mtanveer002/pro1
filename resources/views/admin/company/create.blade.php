@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Add New Company') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form action="{{ route('admin.company.store')}}" method="POST" enctype="multipart/form-data">              
                        @csrf
                       

                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Company Name</label>
                            <input type="text" class="form-control" name="name" id="slider_describtion" placeholder="Company Name" required>
                        </div>
                        @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label"> Email</label>
                            <input type="email" class="form-control" name="email" id="slider_describtion" placeholder="Company Email" required>
                        </div>
                        @error('email')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label"> company Logo</label>
                            <input type="File" class="form-control" name="logo"  required>
                        </div>
                        <div class="mb-3 float-right">
                            <button type="submit" class="btn btn-success">Save</button>
                           
                        </div>
                    </form>

                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
