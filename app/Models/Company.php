<?php

namespace App\Models;

use App\Models\Employees;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Company extends Model
{
    use HasFactory;
    protected $guarded = [];
    
    public function company()
    {
        return $this->hasMany(Employees::class, 'company_id');
    }
    public function image()
    {
        return $this->belongsTo(File::class, 'image_id');
    }

     public function getImage()
    {
        if ( ! $this->image || ! Storage::exists($this->image->getStoragePath())){
            return asset('');
        }
        
        return $this->image->getPath();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
