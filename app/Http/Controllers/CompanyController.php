<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $company = Company::latest()->paginate(10);
        return view('admin.company.index', compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:companies|min:4',
            'email' => 'required',
            
        ]);

        if(request()->hasFile('logo')){
            $image = File::UploadFile(request()->file('logo'));
        }

        $company= New Company();
        $company->name = $request->name;
        $company->email = $request->email;
        $company->image_id = $image->id;
        $company->save();

        return redirect()->route('admin.company.index')->with('success', 'Add Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('admin.company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required|min:4',
            'email' => 'required',
            
        ]);

        if(request()->hasFile('logo')){
            $image = File::UploadFile(request()->file('logo'));
        }

        $company=  Company::find($id);
        $company->name = $request->name;
        $company->email = $request->email;
        $company->image_id = $image->id;
        $company->update();

        return redirect()->route('admin.company.index')->with('success', 'Update Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        $company->delete();

        return redirect()->back()->with('success', 'Delete Successfully');

    }

    public function search(Request $request)
    {
        if($request->ajax()){
            $data = Company::where('name', 'like', '%'.$request->search.'%')->get();

            $output = '';
            if(count($data)>0){

                $output = ' 
            <table class="table">
                <thead>
                    <tr> 
                        <td>#</td>
                        <td>name</td>
                        <td>email</td>
                    </tr>
                </thead>
                <tbody>';
                foreach($data as $row){
                    $output .= '
                    <tr> 
                        <td>' .$row->id. '</td>
                        <td>' .$row->name. '</td>
                        <td>' .$row->email. '<td>
                    </tr>';
                }
                $output.='
                </tbody>
            </table>';

            }else{
                $output.='no result found';
            }

            return $output;
        }
    }
}
