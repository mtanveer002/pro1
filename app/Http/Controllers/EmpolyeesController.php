<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Employees;
use Illuminate\Http\Request;

class EmpolyeesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employees::latest()->paginate(10);
        return view('admin.employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Company::all();
        return view('admin.employees.create', compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $validated = $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',
            
        ]);

     

        $employees= New employees();
        $employees->name = $request->fname;
        $employees->last_name = $request->lname;
        $employees->company_id = $request->company_id;
        $employees->email = $request->email;
        $employees->phone = $request->phone;
        $employees->save();

        return redirect()->route('admin.employees.index')->with('success', 'Add Successfully');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::all();
        $employees = Employees::find($id);
        return view('admin.employees.edit', compact('employees','company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $validated = $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required',
            'phone' => 'required|numeric',
            
        ]);

     

        $employees=  employees::find($id);
        $employees->name = $request->fname;
        $employees->last_name = $request->lname;
        $employees->company_id = $request->company_id;
        $employees->email = $request->email;
        $employees->phone = $request->phone;
        $employees->update();

        return redirect()->route('admin.employees.index')->with('success', 'Update Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employees = Employees::find($id);
        $employees->delete();

        return redirect()->route('admin.employees.index')->with('success', 'Delete Successfully');

    }
}
